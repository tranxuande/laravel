<?php

namespace App\Http\Requests;

use Exception;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class CreateEmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rule = [
            'team_id' => 'required',
            'email' => 'required|email:rfc|unique:App\Models\Employee,email|regex:/^[a-z][a-z0-9]+(\.[_a-z0-9-]+)*@gmail.com$/',
            'first_name' => 'required',
            'last_name' => 'required',
            'gender' => 'required',
            'birthday' => 'required',
            'address' => 'required',
            'salary' => 'required|min:1|integer',
            'position' => 'required',
            'type_of_work' => 'required',
            'status' => 'required',
        ];

        if (!$this->hasFile('avatar') && !session()->has('img')) {
            $rule['avatar'] = 'required|mimes:jpg,bmp,png,jpeg';
        }

        if ($this->hasFile('avatar')) {
            $rule['avatar'] = 'mimes:jpg,bmp,png,jpeg';
        }

        return $rule;
    }

    protected function prepareForValidation()
    {
      
        if ($this->hasFile('avatar')) {
            $img = $this->file('avatar');
            $imgName = time() . '-' . $img->getClientOriginalName();
            $img->storeAs('public/temp/', $imgName);
            $imgUrl = 'storage/temp/' . $imgName;

            session()->put('img', [
                'img_name' => $imgName,
                'img_url' => $imgUrl
            ]);
          
        }
    }

    protected function failedValidation(Validator $validator)
    {
        if ($validator->errors()->has('avatar') && session()->has('img')) {

            Storage::delete(config('const.URL_IMG') . session('img')['img_name']);

            session()->forget('img');
        }
        parent::failedValidation($validator);
    }
}
