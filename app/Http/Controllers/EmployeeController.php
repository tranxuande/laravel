<?php

namespace App\Http\Controllers;

use App\Exports\EmployeesExport;
use App\Http\Requests\CreateEmployeeRequest;
use App\Http\Requests\UpdateEmployeeRequest;
use App\Jobs\SendEmail;
use Illuminate\Http\Request;
use App\Repositories\EmployeeRepository;
use App\Repositories\TeamRepository;
use Exception;
use Excel;
use Illuminate\Support\Facades\Log;

class EmployeeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected $employeeRepository, $teamRepository;

    public function __construct(EmployeeRepository $employeeReposirory, TeamRepository $teamRepository)
    {
        $this->employeeRepository = $employeeReposirory;
        $this->teamRepository = $teamRepository;

        $this->middleware(function ($request, $next) {
            session(['module_active' => 'employee']);

            return $next($request);
        });
    }

    public function index()
    {
        $teams = $this->teamRepository->get();
        $employees = $this->employeeRepository->getAll();
        return view('employee.index', compact('employees', 'teams'));
    }

    public function export()
    {
        $file_name = 'employee.csv';
        try {
            return Excel::download(new EmployeesExport($this->employeeRepository, session('search')), $file_name);
        } catch (Exception $e) {
            $e->getMessage();
        }
        session()->forget('search');
        return redirect()->route('management.employee.index');
    }
    public function reset()
    {
        session()->forget('create_employee');
        session()->forget('img');
        session()->forget('img_update');
        session()->forget('edit_confirm_employee');

        return redirect()->back();
    }

    public function create()
    {
        $teams = $this->teamRepository->get();

        return view('employee.create', compact('teams'));
    }

    public function createConfirm(CreateEmployeeRequest $request)
    {

        $request->flash();

        $employee = $request->except('avatar');

        session()->put('create_employee', $employee);

        $team = $this->teamRepository->findById(session('create_employee')['team_id']);

        return view('employee.create_confirm', compact('team'));
    }

    public function store()
    {
        try {
            $employee = $this->employeeRepository->create(session()->get('create_employee'));

            $data = session()->get('create_employee');

            SendEmail::dispatch($data, $employee)->delay(now()->addMinute(1));

            session()->forget('create_employee');
            session()->forget('img');
        } catch (Exception $e) {
            Log::error('Message: ' . $e->getMessage() . ' Line : ' . $e->getLine());
            return redirect()->route('management.employee.index')->with('message_error', config('message.CREATE_FAIL'));
        }

        return redirect()->route('management.employee.index')->with('message_success', config('message.CREATE_SUCCESS'));
    }

    public function edit($id)
    {
        $employee = $this->employeeRepository->findById($id);

        session()->put('edit_employee', $employee);

        $teams = $this->teamRepository->get();

        if (!$employee) {
            return redirect()->route('management.employee.index')->with('message_error', config('message.FIND_FAIL'));
        }

        return view('employee.update', compact('teams', 'employee'));
    }

    public function editConfirm(UpdateEmployeeRequest $request)
    {
        $request->merge([
            'img_name' => session('edit_employee')['avatar'],
            'img_url' => 'storage/temp/' . session()->get('edit_employee')['avatar']
        ]);

        if (!empty(session('img_update'))) {
            $request->merge(session('img_update'));
        }

        $request->flash();

        $employee = $request->except('avatar');

        session()->put('edit_confirm_employee', $employee);

        $team = $this->teamRepository->findById(session('edit_confirm_employee')['team_id']);

        return view('employee.update_confirm', compact('team'));
    }

    public function update($id)
    {
        try {
            $this->employeeRepository->update($id, session('edit_confirm_employee'));
            session()->forget('edit_confirm_employee');
            session()->forget('img_update');
            session()->forget('edit_employee');
        } catch (Exception $e) {
            Log::error('Message: ' . $e->getMessage() . ' Line : ' . $e->getLine());
            return redirect()->route('management.employee.index')->with('message_error', config('message.UPDATE_FAIL'));
        }

        return redirect()->route('management.employee.index')->with('message_success', config('message.UPDATE_SUCCESS'));
    }

    public function delete()
    {

        $id = request()->employee_id;
        $employee = $this->employeeRepository->findById($id);
        if ($employee) {
            try {
                $this->employeeRepository->delete($id);
            } catch (Exception $e) {            
                Log::error('Message: ' . $e->getMessage() . ' Line : ' . $e->getLine());
                return redirect()->route('management.employee.index')->with('message_error', config('message.DELETE_FAIL'));
            }
            return redirect()->route('management.employee.index')->with('message_success', config('message.DELETE_SUCCESS'));
        } else {
            return redirect()->route('management.employee.index');
        }
    }

    public function search()
    {
        $teams = $this->teamRepository->get();
        session()->put('search', request()->all());

        $employees = $this->employeeRepository->search(request()->all());

        return view('employee.index', compact('employees', 'teams'));
    }
}
