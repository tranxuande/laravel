<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTeamRequest;
use App\Http\Requests\UpdateTeamRequest;
use Illuminate\Http\Request;
use App\Repositories\TeamRepository;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class TeamController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    protected $teamRepository;

    public function __construct(TeamRepository $teamRepository)
    {
        $this->teamRepository = $teamRepository;

        $this->middleware(function ($request, $next) {
            session(['module_active' => 'team']);

            return $next($request);
        });
    }

    public function index()
    {
        $teams = $this->teamRepository->getAll();

        return view('team.index', compact('teams'));
    }

    public function reset()
    {
        session()->forget('add_team');
        session()->forget('edit_team');
        return redirect()->back();
    }

    public function create()
    {
        Log::info("message");
        return view('team.create');
    }

    public function createConfirm(CreateTeamRequest $request)
    {

        $request->flash();

        session()->put('create_team', $request->input());

        return view('team.create_confirm');
    }

    public function store()
    {
        try {
            $this->teamRepository->create(session()->get('create_team'));

            session()->forget('create_team');
        } catch (Exception $e) {
            Log::error('Message: ' . $e->getMessage() . ' Line : ' . $e->getLine());
            return redirect()->route('management.team.index')->with('message_error', config('message.CREATE_FAIL'));
        }

        return redirect()->route('management.team.index')->with('message_success', config('message.CREATE_SUCCESS'));
    }

    public function edit($id)
    {
        $team = $this->teamRepository->findById($id);

        if (!$team) {
            return redirect()->route('management.team.index')->with('message_error', config('message.FIND_FAIL'));
        }

        return view('team.update', compact('team'));
    }

    public function editConfirm(UpdateTeamRequest $request)
    {
        $request->flash();

        session()->put('edit_team', $request->input());

        return view('team.update_confirm');
    }

    public function update($id)
    {
        try {
            $this->teamRepository->update($id, session('edit_team'));

            session()->forget('edit_team');
        } catch (Exception $e) {
            Log::error('Message: ' . $e->getMessage() . ' Line : ' . $e->getLine());
            return redirect()->route('management.team.index')->with('message_error', config('message.UPDATE_FAIL'));
        }

        return redirect()->route('management.team.index')->with('message_success', config('message.UPDATE_SUCCESS'));
    }

    public function delete()
    {
        $id = request()->team_id;
        try {
            $this->teamRepository->delete($id);
        } catch (Exception $e) {
            dd($e->getMessage());
            Log::error('Message: ' . $e->getMessage() . ' Line : ' . $e->getLine());
            return redirect()->route('management.employee.index')->with('message_error', config('message.DELETE_FAIL'));
        }


        return redirect()->route('management.team.index')->with('message_success', config('message.DELETE_SUCCESS'));
    }

    public function search()
    {
        $teams = $this->teamRepository->search(request()->name);

        return view('team.index', compact('teams'));
    }
}
