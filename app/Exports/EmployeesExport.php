<?php

namespace App\Exports;

use App\Models\Employee;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Repositories\EmployeeRepository;

class EmployeesExport implements FromCollection, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    private $employeeRepository, $request;

    public function __construct($employeeRepository, $request)
    {
        $this->employeeRepository = $employeeRepository;
        $this->request = $request;
    }
    public function collection()
    {
        $employees = $this->employeeRepository->getAllWhenExport($this->request);

        $result = [];

        foreach ($employees as $item) {
            $result[] = [
                'id' => $item->id,
                'name' => $item->full_name,
                'email'=> $item->email,
                'gender'=>$item->gender==1 ? 'Male' : 'Female',
            ];
        }

        return collect($result);
    }

    public function headings(): array
    {
        return [
            'id',
            'Name',
            'Email',
            'Gender'
        ];
    }
}
