<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Employee;
use App\Scopes\DelFlagScope;
use Illuminate\Database\Eloquent\Builder;
use Kyslik\ColumnSortable\Sortable;

class Team extends Model
{
    use HasFactory;
    use Sortable;

    protected $table = 'm_teams';

    protected $fillable = [
        'name',
        'ins_id',
        'upd_id',
        'ins_datetime',
        'upd_datetime',
        'del_flag'
    ];

    public $sortable = [
        'id',
        'name',
    ];

    public $timestamps = false;

    public function employees()
    {
        return $this->hasMany(Employee::class);
    }

    protected static function booted()
    {
        static::addGlobalScope(new DelFlagScope);
    }

    public function setNameAttribute($name)
    {
        $this->attributes['name'] = trim($name);
    }
}
