<?php

namespace App\Repositories\interface;

interface RepositoryInterface
{
    public function findById($id);

    public function create($attributes = []);

    public function update($id, array $attribute );

    public function delete($id);
}
