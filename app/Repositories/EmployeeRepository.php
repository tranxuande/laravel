<?php

namespace App\Repositories;

use App\Models\Employee;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class EmployeeRepository extends BaseRepository
{
    public function __construct()
    {
        $this->_model = Employee::class;
    }

    public function getAll()
    {
        return $this->getModel()
            ->select('id', 'team_id', 'email', 'first_name', 'last_name', 'gender', 'birthday', 'address', 'avatar', 'salary', 'position', 'status', 'type_of_work')
            ->sortable()
            ->Paginate(config('const.ITEM_PER_PAGE'));
    }

    public function getAllWhenExport($data)
    {
        // return $this->getModel()
        //     ->select('id', 'team_id', 'email', 'first_name', 'last_name', 'gender', 'birthday', 'address', 'avatar', 'salary', 'position', 'status', 'type_of_work')
        //     ->get();

        return $this->getModel()
            ->when(!empty($data['full_name']), function ($query) use ($data) {
                return $query->where(DB::raw("concat(last_name,' ',first_name)"), 'like', '%' . $data['full_name'] . '%');
                // ->whereRaw("concat(last_name,' ',first_name) like '%?%'", [$data['full_name']]);
            })
            ->when(!empty($data['email']), function ($query) use ($data) {
                return $query->where('email', 'like', '%' . $data['email'] . '%');
            })
            ->when(!empty($data['team_id']), function ($query) use ($data) {
                return $query->where('team_id', $data['team_id']);
            })
            ->get();
    }

    public function create($attributes = [])
    {
        $attributes['avatar'] = session('img')['img_name'];

        return parent::create($attributes);
    }

    public function update($id, $attributes = [])
    {
        if (!empty(session('edit_confirm_employee'))) {
            $attributes['avatar'] = session('edit_confirm_employee')['img_name'];
        }

        return parent::update($id, $attributes);
    }

    public function search($data)
    {

        return $this->getModel()->sortable()
            ->when(!empty($data['full_name']), function ($query) use ($data) {
                return $query->where(DB::raw("concat(last_name,' ',first_name)"), 'like', '%' . $data['full_name'] . '%');
                // ->whereRaw("concat(last_name,' ',first_name) like '%?%'", [$data['full_name']]);
            })
            ->when(!empty($data['email']), function ($query) use ($data) {
                return $query->where('email', 'like', '%' . $data['email'] . '%');
            })
            ->when(!empty($data['team_id']), function ($query) use ($data) {
                return $query->where('team_id', $data['team_id']);
            })
            ->paginate(config('const.ITEM_PER_PAGE'));
    }
}
