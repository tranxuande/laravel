<?php

namespace App\Repositories;

use App\Models\Team;
use Exception;
use Illuminate\Support\Facades\DB;


class TeamRepository extends BaseRepository
{
    public function __construct()
    {
        $this->_model = Team::class;
    }

    public function getAll()
    {
        return $this->getModel()
        ->sortable()
        ->paginate(config('const.ITEM_PER_PAGE'));
    }

    public function get()
    {
        return $this->getModel()->select('id', 'name')->get();
    }

    public function search($name)
    {
        return $this->getModel()
        ->sortable()
        ->where('name', 'like', '%' . $name . '%')
        ->paginate(config('const.ITEM_PER_PAGE'));
    }

    public function delete($id)
    {
        DB::beginTransaction();
        $data=[];
        try {
            parent::delete($id);

            DB::table('m_employees')->where('team_id', $id)->update(['del_flag' => config('const.DELETE_ON')]);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            throw new Exception($e->getMessage());
        }
    }
}
