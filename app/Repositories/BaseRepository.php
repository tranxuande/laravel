<?php

namespace app\Repositories;

use App\Repositories\interface\RepositoryInterface;
use Illuminate\Support\Facades\Auth;

abstract class BaseRepository implements RepositoryInterface
{
    protected $_model;

    public function getModel()
    {
        return app()->make($this->_model);
    }

    public function findById($id)
    {
        $result = $this->getModel()->where('id', $id)->first();

        return $result;
    }

    public function create($attributes = [])
    {
        $attributes['ins_id'] = Auth::id();
        $attributes['ins_datetime'] = date('Y-m-d H:i:s');

        return $this->getModel()->create($attributes);
    }

    public function update($id, $attributes = [])
    {
        $attributes['upd_id'] = Auth::id();
        $attributes['upd_datetime'] = date("Y-m-d H:i:s");

        return ($this->getModel()->find($id))->update($attributes);
    }

    public function delete($id)
    {        
        $data = [
            'del_flag' => config('const.DELETE_ON')
        ];

        $this->update($id, $data);
    }
}
