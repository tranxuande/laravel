<?php

return [
    'DELETE_OFF' => 0,
    'DELETE_ON' => 1,

    'ITEM_PER_PAGE' => 3,

    'GENDER_MALE' => 1,
    'GENDER_FEMALE' => 2,

    'POSITION_MANAGER' => 1,
    'POSITION_TEAM_LEADER' => 2,
    'POSITION_BSE' => 3,
    'POSITION_DEV' => 4,
    'POSITION_TESTER' => 5,

    'STATUS_ON_WORKING' => 1,
    'STATUS_RETIRED' => 2,

    'TYPE_OF_WORK_FULL_TIME' => 1,
    'TYPE_OF_WORK_PART_TIME' => 2,
    'TYPE_OF_WORK_PROBATIONARY_STAFF' => 3,
    'TYPE_OF_WORK_INTERN' => 4,

    "URL_IMG" => 'storage/temp/',
];
