<?php

return [
    'CREATE_SUCCESS' => 'Create success',
    'CREATE_FAIL' => 'Create fail',

    'FIND_FAIL' => 'Not find record',

    'UPDATE_SUCCESS' => 'Update success',
    'UPDATE_FAIL' => 'Update fail',

    'DELETE_SUCCESS' => 'Delete success',
    'DELETE_FAIL' => 'Delete fail'
];
