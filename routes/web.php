<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\TeamController;
use App\Http\Middleware\CheckLogin;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('', function () {
    return redirect()->route('management.team.index');
})->middleware('auth.login');

Route::get('/login', [AuthController::class, 'login'])->name('login');
Route::post('/postLogin', [AuthController::class, 'postLogin'])->name('postLogin');
Route::get('logout', [AuthController::class, 'logout'])->name('logout');

// Route::middleware('auth.login')->prefix('management')->group(function () {
//     Route::prefix('team')->group(function () {
//         Route::get('', [TeamController::class, 'index'])->name('management.team.index');
//         Route::get('reset', [TeamController::class, 'reset'])->name('management.team.reset');

//         Route::get('create', [TeamController::class, 'create'])->name('management.team.create');
//         Route::post('create_confirm', [TeamController::class, 'createConfirm'])->name('management.team.create_confirm');
//         Route::post('store', [TeamController::class, 'store'])->name('management.team.store');

//         Route::get('edit/{id}', [TeamController::class, 'edit'])->name('management.team.edit');
//         Route::post('edit_confirm/{id}', [TeamController::class, 'editConfirm'])->name('management.team.edit_confirm');
//         Route::post('update/{id}', [TeamController::class, 'update'])->name('management.team.update');

//         Route::post('delete/{id}', [TeamController::class, 'delete'])->name('management.team.delete');

//         Route::get('search', [TeamController::class, 'search'])->name('management.team.search');
//     });

//     Route::prefix('employee')->group(function () {
//         Route::get('', [EmployeeController::class, 'index'])->name('management.employee.index');
//         Route::get('reset', [EmployeeController::class, 'reset'])->name('management.employee.reset');
//         Route::get('export', [EmployeeController::class, 'export'])->name('management.employee.export');

//         Route::get('create', [EmployeeController::class, 'create'])->name('management.employee.create');
//         Route::post('create_confirm', [EmployeeController::class, 'createConfirm'])->name('management.employee.create_confirm');
//         Route::post('store', [EmployeeController::class, 'store'])->name('management.employee.store');

//         Route::get('edit/{id}',[EmployeeController::class, 'edit'])->name('management.employee.edit');
//         Route::post('edit_confirm/{id}',[EmployeeController::class, 'editConfirm'])->name('management.employee.edit_confirm');
//         Route::post('update/{id}',[EmployeeController::class, 'update'])->name('management.employee.update');

//         Route::post('delete/{id}',[EmployeeController::class, 'delete'])->name('management.employee.delete');

//         Route::get('search', [EmployeeController::class, 'search'])->name('management.employee.search');
//     });
// });
Route::name('management.')->middleware('auth.login')->prefix('management')->group(function () {
    
    Route::name('team.')->controller(TeamController::class)->prefix('team')->group(function () {
        Route::get('', 'index')->name('index');
        Route::get('reset', 'reset')->name('reset');

        Route::get('create', 'create')->name('create');
        Route::post('create_confirm', 'createConfirm')->name('create_confirm');
        Route::post('store', 'store')->name('store');

        Route::get('edit/{id}', 'edit')->name('edit');
        Route::post('edit_confirm/{id}', 'editConfirm')->name('edit_confirm');
        Route::post('update/{id}', 'update')->name('update');

        Route::post('delete/{id}', 'delete')->name('delete');

        Route::get('search', 'search')->name('search');
    });

    Route::name('employee.')->controller(EmployeeController::class)->prefix('employee')->group(function () {
        Route::get('', 'index')->name('index');
        Route::get('reset', 'reset')->name('reset');
        Route::get('export', 'export')->name('export');

        Route::get('create', 'create')->name('create');
        Route::post('create_confirm',  'createConfirm')->name('create_confirm');
        Route::post('store', 'store')->name('store');

        Route::get('edit/{id}', 'edit')->name('edit');
        Route::post('edit_confirm/{id}', 'editConfirm')->name('edit_confirm');
        Route::post('update/{id}', 'update')->name('update');

        Route::post('delete/{id}', 'delete')->name('delete');

        Route::get('search', 'search')->name('search');
    });
});
