<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" />
    <title>Document</title>
    <style>
        body {
            padding: 0;
            margin: 0;
        }

        .menu {
            margin-bottom: 30px;
            top: 0;
            left: 0;
            padding: 0 3rem;
            background-color: whitesmoke;
            display: flex;
            justify-content: right;
            flex-direction: row;
            align-items: center;
            color: black;

        }

        .menu a {
            text-decoration: none;
            margin: 0 1rem;
            padding: 1rem .5rem;
            color: black;
        }

        .dropdown {
            position: relative;
        }

        .dropdown ul {
            width: 200px;
            text-align: center;
            position: absolute;
            background: aliceblue;
            padding: 0;
            list-style-type: none;
            margin-top: 10px;
            display: none;
        }

        .dropdown ul li {
            padding: .5rem;
        }

        .dropdown ul li:hover {
            background-color: antiquewhite;

        }

        .dropdown ul li a {
            width: 100%;
            height: 100%;
            display: inline-block;
            padding: 0;
            margin: 0;
        }

        .dropdown a:focus+ul,
        .dropdown a:hover+ul,
        .dropdown ul:hover {
            display: block;
        }

        .active-menu {
            background-color: white;
            padding: 16px;
        }

        .active-menu a {
            color: red;
        }

        .icon {
            font-size: 12px;
            margin-left: 6px;
        }

       
    </style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
</head>

<body>
    <nav class="menu">
        <div class="dropdown {{session('module_active')=='team' ? 'active-menu' : ''}}">
            <a href="{{route('management.team.index')}}">Team <i class="fa fa-chevron-down icon" aria-hidden="true"></i></a>
            <ul>
                <li>
                    <a href="{{ route('management.team.create') }}">Create Team</a>
                </li>
                <li>
                    <a href="">Search Team</a>
                </li>
            </ul>
        </div>

        <div class="dropdown {{session('module_active')=='employee' ? 'active-menu' : ''}}">
            <a href="{{route('management.employee.index')}}">Employee<i class="fa fa-chevron-down icon" aria-hidden="true"></i></a>
            <ul>
                <li>
                    <a href="{{route('management.employee.create')}}">Create Employee</a>
                </li>
                <li>
                    <a href="">Search Employee</a>
                </li>
            </ul>
        </div>
        <a href="{{ route('logout') }}">Logout</a>
    </nav>

    @if(session('message_success'))
    <div class="alert alert-success" role="alert">
        {{session('message_success')}}
    </div>
    @endif

    @if(session('message_error'))
    <div class="alert alert-danger" role="alert">
        {{session('message_error')}}
    </div>
    @endif
    <div>
        @yield('content')
    </div>
</body>

</html>
<script>
    var loadFile = function(event) {
        var output = document.getElementById('output');
        output.src = URL.createObjectURL(event.target.files[0]);
        output.onload = function() {
            URL.revokeObjectURL(output.src)
        }
    };
</script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>