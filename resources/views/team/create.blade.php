@extends('layout.app')

@section('content')
<link href="{{ asset('css/team.css') }}" rel="stylesheet">

<div class="create">
    <h3>Create team</h3>
    <form action="{{ route('management.team.create_confirm') }}" method="post">
        @csrf
        <div id="create-input">
            <label>Name</label>
            <span>
                <input name="name" value="{{ old('name') }}" />
                <div style="color: red">
                    @error('name')
                    {{$message}}
                    @enderror
                </div>
        </div>
        <div class="submit-form">
            <a href="{{ route('management.team.reset') }}">
                <div id="reset">
                    Reset
                </div>
            </a>
            <button type="submit" class="btn btn-primary">Confirm</button>
        </div>
    </form>
</div>


</div>

@endsection