@extends('layout.app')

@section('content')
<link href="{{ asset('css/team.css') }}" rel="stylesheet">

<div id="confirm">
    <h3>Update confirm</h3>
    <div id="create-team-confirm">
        Name: {{session('edit_team')['name']}}
    </div>

    

    <div class="submit-form">
        <a href="{{ url()->previous() }}">
            <div id="reset">
                Back
            </div>
        </a>
        <button type="button" class="btn btn-primary" style="margin-left: 30px;" data-toggle="modal" data-target="#exampleModal">Update</button>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Confirm Update</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Are you sure ?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <form action="{{ route('management.team.update', request()->id) }}" method="post">
                        @csrf
                        <button type="submit" class="btn btn-primary">Save</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection