@extends('layout.app')

@section('content')
<link href="{{ asset('css/team.css') }}" rel="stylesheet">

<div id="update-form">
    <h3>Update team</h3>
    <form action="{{ route('management.team.edit_confirm', $team->id) }}" method="post">
        @csrf
        <div id="create-input">
            <div class="up">
                ID: <span>{{$team->id}}</span>
            </div>
            <div class="up">
                Name <input name="name" value="{{old('name', isset($team)? $team->name : '')}}" />
                <div style="color: red">
                    @error('name')
                    {{$message}}
                    @enderror
                </div>
            </div>

        </div>
        <div class="submit-form">
            <a href="{{ route('management.team.reset') }}">
                <div id="reset">
                    Reset
                </div>
            </a>
            <button type="submit" class="btn btn-primary">Confirm</button>
        </div>
    </form>
</div>


@endsection