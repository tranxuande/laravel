@extends('layout.app')

@section('content')
<link href="{{ asset('css/team.css') }}" rel="stylesheet">

<div class="form">
    <form action="{{route('management.team.search')}}" method="get">
        <input name="name" value="{{request()->name}}" placeholder="name..." />
        <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
    </form>
    <span id="reset">
        <a href="{{route('management.team.index')}}"><i class="fa fa-times" aria-hidden="true"></i></a>
    </span>
</div>


<div id="table-team">
    <table class="table table-striped" style="border-top: none;">
        <thead>
            <tr>
                <th scope="col">@sortablelink('id','ID')</th>
                <th scope="col">@sortablelink('name','Name')</th>
                
            </tr>
        </thead>
        <tbody>
            @if(count($teams)>0)
            @foreach($teams as $team)
            <tr>
                <th scope="row">{{$team->id}}</th>
                <td>{{$team->name}}</td>
                <td>
                    <a href="{{ route('management.team.edit', $team->id) }}" id="update">Update</a>
                    <a href="" data-toggle="modal" data-target="#exampleModal" team_id="{{$team->id}}" id="delete">Delete</a>
                </td>
                
            </tr>

            @endforeach
            @else
            <td colspan="3" id="no-record">No record!!!</td>
            @endif
        </tbody>
    </table>
</div>

<div id="paginate">
    {{ $teams->appends(request()->all())->links() }}
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Confirm Create</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Are you sure ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <form action="{{route('management.team.delete','id')}}" method="post">
                    @csrf
                    <input type="hidden" name="team_id">
                    <button type="submit" class="btn btn-primary">Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(function() {
        $('a#delete').click(
            function() {
                let team_id = $(this).attr('team_id');
                $('input[name="team_id"]').val(team_id);
            }
        )
    })
</script>
@endsection