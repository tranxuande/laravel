@extends('layout.app')

@section('content')
<link href="{{ asset('css/employee.css') }}" rel="stylesheet">
<style>
    #confirm {
        width: 600px;
        margin: 20px auto;
        border: 1px solid black;
        padding: 30px;
    }

    .confirm_input {
        margin-bottom: 20px;
    }

    #form {
        margin-top: 30px;
    }

    .submit-form {
        display: flex;
        justify-content: space-between;
    }

    .column {
        padding-left: 50px;
    }

    td {
        padding-bottom: 30px;
    }

    .submit-form #reset {
        width: 100px;
        height: 40px;
        background-color: darksalmon;
        color: white;
        text-align: center;
        border-radius: 3px;
        padding-top: 7px;
    }

    .submit-form a:hover {
        text-decoration: none;
    }
</style>


<!---------------------------->
<div id="confirm">

    <div id="form-create-conf">
        <h3>
            Update confirm emloyee
        </h3>
        <div id="form-conf">

            <table>
                <tr>
                    <td>Avartar</td>
                    <td class="column">
                        <img height="200px" width="200px" src="{{asset(session('edit_confirm_employee')['img_url'])}}" />
                    </td>
                </tr>

                <tr>
                    <td>Team</td>
                    <td class="column">
                        {{ $team->name }}
                    </td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td class="column">
                        {{session('edit_confirm_employee')['email']}}
                    </td>
                </tr>
                <tr>
                    <td>First name</td>
                    <td class="column">
                        {{session('edit_confirm_employee')['first_name']}}
                    </td>
                </tr>
                <tr>
                    <td>Last name</td>
                    <td class="column">
                        {{session('edit_confirm_employee')['last_name']}}
                    </td>
                </tr>
                <tr>
                    <td>Gender</td>
                    <td class="column">
                        {{session('edit_confirm_employee')['gender']==1 ? 'Male' : 'Female'}}
                    </td>
                </tr>
                <tr>
                    <td>Birthday</td>
                    <td class="column">
                        {{session('edit_confirm_employee')['birthday']}}
                    </td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td class="column">
                        {{session('edit_confirm_employee')['address']}}
                    </td>
                </tr>
                <tr>
                    <td>Salary</td>
                    <td class="column">
                        {{session('edit_confirm_employee')['salary']}}
                    </td>
                </tr>
                <tr>
                    <td>Position</td>
                    <td class="column">
                        @switch( session('edit_confirm_employee')['position'] )
                        @case(config('const.POSITION_MANAGER'))
                        Manager
                        @break
                        @case(config('const.POSITION_TEAM_LEADER'))
                        Team leader
                        @break
                        @case(config('const.POSITION_BSE'))
                        BSE
                        @break
                        @case(config('const.POSITION_DEV'))
                        Dev
                        @break
                        @case(config('const.POSITION_TESTER'))
                        Tester
                        @break
                        @endswitch
                    </td>
                </tr>
                <tr>
                    <td>Type of work</td>
                    <td class="column">
                        @switch(session('edit_confirm_employee')['type_of_work'])
                        @case(config('const.TYPE_OF_WORK_FULL_TIME'))
                        Fulltime
                        @break
                        @case(config('const.TYPE_OF_WORK_PART_TIME'))
                        Parttime
                        @break
                        @case(config('const.TYPE_OF_WORK_PROBATIONARY_STAFF'))
                        Probationary staff
                        @break
                        @case(config('const.TYPE_OF_WORK_INTERN'))
                        Intern
                        @break
                        @endswitch
                    </td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td class="column">
                        {{session('edit_confirm_employee')['status']==1 ? 'Working' : 'Retired'}}
                    </td>
                </tr>
            </table>

            <div class="submit-form">
                <a href="{{ url()->previous() }}">
                    <div id="reset">
                        Back
                    </div>
                </a>
                <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Update</button>
            </div>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Confirm Create</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Are you sure ?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <form action="{{route('management.employee.update', request()->id)}}" method="post">
                        @csrf
                        <button type="submit" class="btn btn-primary">Save</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection