@extends('layout.app')

@section('content')

<link href="{{ asset('css/employee.css') }}" rel="stylesheet">

<div id="confirm">
    
    <div id="form-create-conf">
        <h3>
            Create confirm emloyee
        </h3>
        <div id="form-conf">

            <table>
                <tr>
                    <td>Avartar</td>
                    <td class="column">
                        <img height="200px" width="200px" src="{{asset(session('img')['img_url']) }}" />
                    </td>
                </tr>

                <tr>
                    <td>Team</td>
                    <td class="column">
                        {{ $team->name }}
                    </td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td class="column">
                        {{session('create_employee')['email']}}
                    </td>
                </tr>
                <tr>
                    <td>First name</td>
                    <td class="column">
                        {{session('create_employee')['first_name']}}
                    </td>
                </tr>
                <tr>
                    <td>Last name</td>
                    <td class="column">
                        {{session('create_employee')['last_name']}}
                    </td>
                </tr>
                <tr>
                    <td>Gender</td>
                    <td class="column">
                        {{session('create_employee')['gender']==1 ? 'Male' : 'Female'}}
                    </td>
                </tr>
                <tr>
                    <td>Birthday</td>
                    <td class="column">
                        {{session('create_employee')['birthday']}}
                    </td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td class="column">
                        {{session('create_employee')['address']}}
                    </td>
                </tr>
                <tr>
                    <td>Salary</td>
                    <td class="column">
                        {{session('create_employee')['salary']}}
                    </td>
                </tr>
                <tr>
                    <td>Position</td>
                    <td class="column">
                        @switch( session('create_employee')['position'] )
                        @case(config('const.POSITION_MANAGER'))
                        Manager
                        @break
                        @case(config('const.POSITION_TEAM_LEADER'))
                        Team leader
                        @break
                        @case(config('const.POSITION_BSE'))
                        BSE
                        @break
                        @case(config('const.POSITION_DEV'))
                        Dev
                        @break
                        @case(config('const.POSITION_TESTER'))
                        Tester
                        @break
                        @endswitch
                    </td>
                </tr>
                <tr>
                    <td>Type of work</td>
                    <td class="column">
                        @switch(session('create_employee')['type_of_work'])
                        @case(config('const.TYPE_OF_WORK_FULL_TIME'))
                        Fulltime
                        @break
                        @case(config('const.TYPE_OF_WORK_PART_TIME'))
                        Parttime
                        @break
                        @case(config('const.TYPE_OF_WORK_PROBATIONARY_STAFF'))
                        Probationary staff
                        @break
                        @case(config('const.TYPE_OF_WORK_INTERN'))
                        Intern
                        @break
                        @endswitch
                    </td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td class="column">
                        {{session('create_employee')['status']==1 ? 'Working' : 'Retired'}}
                    </td>
                </tr>
            </table>

            <div class="submit-form">
                <a href="{{ url()->previous() }}">
                    <div id="reset">
                        Back
                    </div>
                </a>
                <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Create</button>
            </div>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Confirm Create</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Are you sure ?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <form action="{{route('management.employee.store')}}" method="post">
                        @csrf
                        <button type="submit" class="btn btn-primary">Save</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection