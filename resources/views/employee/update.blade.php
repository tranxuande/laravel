@extends('layout.app')

@section('content')
<link href="{{ asset('css/employee.css') }}" rel="stylesheet">


<div id="form-create">
    <h3>
        Update emloyee
    </h3>
    <div id="form">
        <form action="{{ route('management.employee.edit_confirm', $employee->id) }}" method="post" enctype="multipart/form-data">
            @csrf
            <table>
                <tr>
                    <td>Avartar</td>
                    <td class="column">
                        <input type="file" name='avatar' onchange="loadFile(event)" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td class="column">
                        <img id="output" height="200px" width="200px" src="{{ asset( !empty(session('edit_confirm_employee')) ? session('edit_confirm_employee')['img_url'] : ( !empty(session('img_update')) ? session('img_update')['img_url'] :  config('const.URL_IMG').$employee->avatar) ) }}" />
                        <div style="color: red">
                            @error('avatar')
                            {{$message}}
                            @enderror
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Team</td>
                    <td class="column">
                        <select name="team_id">
                            @if(count($teams)>0)
                            @foreach($teams as $team)
                            <option value="{{$team->id}}" {{old('team_id', isset($employee) ? $employee->team_id : '') == $team->id ? 'selected' : ''}}> {{$team->name}} </option>
                            @endforeach
                            @endif
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td class="column">
                        <input name='email' value="{{old('email' , session('edit_employee')['email'])}}"  />
                        <div style="color: red">
                            @error('email')
                            {{$message}}
                            @enderror
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>First name</td>
                    <td class="column">
                        <input name='first_name' value="{{old('first_name' , $employee->first_name)}}" />
                        <div style="color: red">
                            @error('first_name')
                            {{$message}}
                            @enderror
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Last name</td>
                    <td class="column">
                        <input name='last_name' value="{{old('last_name' , $employee->last_name)}}" />
                        <div style="color: red">
                            @error('last_name')
                            {{$message}}
                            @enderror
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Gender</td>
                    <td class="column">
                        <input type="radio" name="gender" value="{{config('const.GENDER_MALE')}}" {{old('gender', isset($employee) ? $employee->gender : '') == config('const.GENDER_MALE') ? 'checked' : '' }} />Male
                        <input type="radio" name="gender" value="{{config('const.GENDER_FEMALE')}}" {{old('gender', isset($employee) ? $employee->gender : '') == config('const.GENDER_FEMALE') ? 'checked' : '' }} />Female
                    </td>
                </tr>
                <tr>
                    <td>Birthday</td>
                    <td class="column">
                        <input type="date" name='birthday' value="{{old('birthday' , $employee->birthday)}}" />
                        <div style="color: red">
                            @error('birthday')
                            {{$message}}
                            @enderror
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td class="column">
                        <input name='address' value="{{old('address' , $employee->address)}}"  />
                        <div style="color: red">
                            @error('address')
                            {{$message}}
                            @enderror
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Salary</td>
                    <td class="column">
                        <input name='salary' value="{{old('salary' , $employee->salary)}}" />VND
                        <div style="color: red">
                            @error('salary')
                            {{$message}}
                            @enderror
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Position</td>
                    <td class="column">
                        <?php $listPosition = [
                            config('const.POSITION_MANAGER') => 'Manager',
                            config('const.POSITION_TEAM_LEADER') => 'Team leader',
                            config('const.POSITION_BSE') => 'BSE',
                            config('const.POSITION_DEV') => 'Dev',
                            config('const.POSITION_TESTER') => 'Tester',
                        ]; ?>
                        <select name="position">
                            @foreach($listPosition as $key =>$value)
                            <option value="{{$key}}" {{old('position', isset($employee) ? $employee->position : '') == $key ? 'selected' : ''}}>{{$value}}</option>
                            @endforeach
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Type of work</td>
                    <td class="column">
                        <?php $listTypeOfWork = [
                            config('const.TYPE_OF_WORK_FULL_TIME') => 'FullTime',
                            config('const.TYPE_OF_WORK_PART_TIME') => 'Parttime',
                            config('const.TYPE_OF_WORK_PROBATIONARY_STAFF') => 'Probationary staff',
                            config('const.TYPE_OF_WORK_INTERN') => 'Intern',
                        ]; ?>
                        <select name="type_of_work">
                            @foreach($listTypeOfWork as $key =>$value)
                            <option value="{{$key}}" {{old('type_of_work', isset($employee) ? $employee->type_of_work : '') == $key ? 'selected' : ''}}>{{$value}}</option>
                            @endforeach
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td class="column">
                        <input type="radio" name="status" value="{{config('const.STATUS_ON_WORKING')}}" {{old('status', isset($employee) ? $employee->status : '') == config('const.STATUS_ON_WORKING') ? 'checked' : '' }} />Working
                        <input type="radio" name="status" value="{{config('const.STATUS_RETIRED')}}" {{old('status', isset($employee) ? $employee->status : '') == config('const.STATUS_RETIRED') ? 'checked' : '' }} />Retired
                        <div style="color: red">
                            @error('status')
                            {{$message}}
                            @enderror
                        </div>
                    </td>
                </tr>
            </table>

            <div class="submit-form">
                <a href="{{ route('management.employee.reset') }}">
                    <div id="reset">
                        Reset
                    </div>
                </a>
                <button type="submit" class="btn btn-primary">Confirm</button>
            </div>
        </form>


    </div>
</div>
@endsection