@extends('layout.app')

@section('content')
<link href="{{ asset('css/employee.css') }}" rel="stylesheet">
<style>
    #search td{
        padding-left: 30px;
    }
</style>
<div id="search">
    <form action="{{ route('management.employee.search') }}" method="get">
        <table >
            <tr>
                <td>Team</td>
                <td class="input">
                    <select name="team_id"  >
                        @foreach($teams as $team)
                        <option value="{{ $team->id }}" {{request()->team_id== $team->id ? 'selected' : ''}}>{{ $team->name }}</option>
                        @endforeach
                    </select>
                </td>
            </tr>
            <tr>
                <td>Name</td>
                <td  class="input">
                    <input name="full_name" value="{{request()->full_name}}" />
                </td>
            </tr>
            <tr>
                <td>Email</td>
                <td  class="input">
                    <input name="email" value="{{request()->email}}"  />
                </td>
            </tr>
        </table>
        <div class="submit-form">
            <a href="{{route('management.employee.index')}}">
                <div id="reset">
                    Reset
                </div>
            </a>
            <button type="submit" class="btn btn-primary">Search</button>
        </div>
    </form>
</div>

<div id="table-emp">
    <div id="export">
        <a href="{{route('management.employee.export')}}">
            <div id="export-link">Export</div>
        </a>
    </div>
    <table class="table table-hover table-borderless" style="border-top: none;">
        <thead>
            <tr>
                <th scope="col">@if(count($employees)>1) @sortablelink('id','ID') @else ID @endif</th>
                <th scope="col">Avatar</th>
                <th scope="col">Team</th>
                <th scope="col">@if(count($employees)>1) @sortablelink('email','Email') @else Email @endif</th>
                <th scope="col">@if(count($employees)>1) @sortablelink('first_name','Name') @else Name @endif</th>
                <th scope="col">Gender</th>
                <th scope="col">Birthday</th>
                <th scope="col">@if(count($employees)>1) @sortablelink('addresss','Addresss') @else Addresss @endif</th>
                <th scope="col">Salary</th>
                <th scope="col">Position</th>
                <th scope="col">Status</th>
                <th scope="col">Type of work</th>
            </tr>
        </thead>
        <tbody>
            @if(count($employees)>0)
            @foreach($employees as $employee)
            <tr>
                <th scope="row">{{$employee->id}}</th>
                <td>
                    <img height="100px" width="100px" src="{{asset(config('const.URL_IMG').$employee->avatar)}}" />
                </td>
                <td>{{$employee->team->name}}</td>
                <td>{{$employee->email}}</td>
                <td>{{$employee->full_name}}</td>
                <td>{{$employee->gender == config('const.GENDER_MALE') ? 'Male' : 'Female'}}</td>
                <td>{{$employee->birthday}}</td>
                <td>{{$employee->address}}</td>
                <td>{{$employee->salary}}</td>
                <td>
                    @switch($employee->position)
                    @case(config('const.POSITION_MANAGER'))
                    Manager
                    @break
                    @case(config('const.POSITION_TEAM_LEADER'))
                    Team leader
                    @break
                    @case(config('const.POSITION_BSE'))
                    BSE
                    @break
                    @case(config('const.POSITION_DEV'))
                    Dev
                    @break
                    @case(config('const.POSITION_TESTER'))
                    Tester
                    @break
                    @endswitch
                </td>
                <td>{{$employee->status== config('const.STATUS_ON_WORKING') ? 'On working' : 'Retired'}}</td>
                <td>
                    @switch($employee->type_of_work)
                    @case(config('const.TYPE_OF_WORK_FULL_TIME'))
                    Fulltime
                    @break
                    @case(config('const.TYPE_OF_WORK_PART_TIME'))
                    Parttime
                    @break
                    @case(config('const.TYPE_OF_WORK_PROBATIONARY_STAFF'))
                    Probationary staff
                    @break
                    @case(config('const.TYPE_OF_WORK_INTERN'))
                    Intern
                    @break
                    @endswitch
                </td>
                <td><a href="{{ route('management.employee.edit', $employee->id) }}">Update</a></td>
                <td><a href="" data-toggle="modal" data-target="#exampleModal" employee_id="{{$employee->id}}" id="delete">Delete</a></td>
            </tr>
            @endforeach
            @else
            <td colspan="12" id="no-record">No record!!!</td>
            @endif
        </tbody>
    </table>

    <div class="text-center" style="margin:0 auto; width: auto">
        {{ $employees->appends(request()->all())->links() }}
    </div>
</div>

<div class="modal fade" id="exampleModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Confirm Delete</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Are you sure ?
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <form action="{{route('management.employee.delete','id' )}}" method="post">
                    @csrf
                    <input type="hidden" name="employee_id" value="">
                    <button type="submit" class="btn btn-primary">Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(function() {
        $('a#delete').click(
            function() {
                let employee_id = $(this).attr('employee_id');
                $('input[name="employee_id"]').val(employee_id);
            }
        )
    })
</script>
@endsection